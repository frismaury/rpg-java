/** @author arkalab | 09/15/14
 *  info:
 *
 */

package gamerpg;

import gamerpg.entities.Player;
import gamerpg.gfx.ImageLoader;
import gamerpg.gfx.ImageManager;
import gamerpg.gfx.SpriteSheet;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;



public class GameRPG extends Canvas implements Runnable {
    private static final long serialVersionUID = 1L; //wtf
    public static final int WIDTH = 200, HEIGHT = 200, SCALE = 2; //tamaño de la ventana
    public static boolean running = false;

    public Thread gameThread;
    
    private BufferedImage spriteSheet;
    private ImageManager im;
    
    private static Player player;
    
    public void init(){ //inicializador de los objetos
        ImageLoader loader = new ImageLoader();
        spriteSheet = loader.load("/spritesheet.png");
        
        SpriteSheet ss = new SpriteSheet(spriteSheet);
        
        im = new ImageManager(ss);
        
        player = new Player(0, 0, im); //asignandole una forma del spritesheet
        //para que escuche el teclado
        this.addKeyListener(new KeyManager());
        
    }
    
    public synchronized void start(){
        if(running){ // si ya esta corriendo que no haga nada
            return;
        }else{
            running = true;
            gameThread = new Thread(this);
            gameThread.start();
        }
    }
    
    public synchronized void stop(){
        if(!running){ // si ya esta corriendo que no haga nada
            return;
        }else{
            running = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
        }
    }      
    
    public void run(){ //loop para el juego
        init();
        long lastTime = System.nanoTime();
        final double amountOfTicks = 60D; //velocidad de actualizacion de los objetos 
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        
        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            
            if(delta >= 1){ //limitacion de los ticks a 60 por segundo (podrían ser fps)
                tick();
                delta--;
            }
            render(); 
        }
        stop();
    }
    
    public void tick(){ //actualizador de todos los objetos de la pantalla
        player.tick();
    }
    
    public void render(){ // dibujador de todo los objetos de fondo
        BufferStrategy bs = getBufferStrategy();
        
        if(bs == null){ 
            createBufferStrategy(3); //servirá para el "smooth" dibujado de las imagenes en la pantalla y !> 3
            return;
        }
        
        Graphics g = bs.getDrawGraphics(); // g = graphic
        //inicio del render
            g.fillRect(0, 0, WIDTH * SCALE, HEIGHT * SCALE);
            player.render(g);
        //final del render       
        g.dispose();
        bs.show();
    }

    public static void main(String[] args) {
        
        GameRPG game = new GameRPG();
        game.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        game.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        game.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        
        
        JFrame frame = new JFrame("RPG Proyect");
        frame.setSize(WIDTH * SCALE, HEIGHT * SCALE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.add(game);
        frame.setVisible(true);
        
        game.start();
    }
    
    
    public static Player getPlayer(){
        return player;
    }

}

/** POR HACER
 *
 */