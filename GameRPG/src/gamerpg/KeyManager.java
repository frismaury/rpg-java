// @author frismaury / @date 09/16/2014

package gamerpg;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



public class KeyManager implements KeyListener{
    
    public KeyManager(){
        
    }
    
    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_UP){
            GameRPG.getPlayer().up = true;            
        }
        
        if(ke.getKeyCode() == KeyEvent.VK_DOWN){
            GameRPG.getPlayer().dn = true;
        }
         
        if(ke.getKeyCode() == KeyEvent.VK_LEFT){
            GameRPG.getPlayer().lt = true;
        }
          
        if(ke.getKeyCode() == KeyEvent.VK_RIGHT){
            GameRPG.getPlayer().rt = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
       if(ke.getKeyCode() == KeyEvent.VK_UP){
            up = false;
        }
        
        if(ke.getKeyCode() == KeyEvent.VK_DOWN){
            dn = false;
        }
         
        if(ke.getKeyCode() == KeyEvent.VK_LEFT){
            lt = false;
        }
          
        if(ke.getKeyCode() == KeyEvent.VK_RIGHT){
            rt = false;
        } 
    }
}

/** TODO / NOTAS **
 *
 */