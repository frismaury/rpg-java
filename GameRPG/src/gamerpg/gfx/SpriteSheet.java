// @author frismaury / @date 09/16/2014

package gamerpg.gfx;

import java.awt.image.BufferedImage;


public class SpriteSheet {
    
    private BufferedImage sheet;
    
    public SpriteSheet(BufferedImage sheet){
        this.sheet = sheet;
    }
    
    public BufferedImage crop(int col, int row, int w, int h){ // w idth  h eight
        return sheet.getSubimage(col * 16, row * 16, w, h);
    }
}

/** TODO / NOTAS **
 *
 */