// @author frismaury / @date 09/16/2014

package gamerpg.entities;


import gamerpg.GameRPG;
import gamerpg.gfx.ImageManager;
import gamerpg.gfx.SpriteSheet;
import java.awt.Graphics;


public class Player {
    
    private int x, y;
    private ImageManager im;
    public boolean up = false, dn = false, lt = false, rt = false;
    private final int SPEED = 3; //cantidad  de pixeles a mover
    
    public Player(int x, int y, ImageManager im){
        this.x = x;
        this.y = y;
        this.im = im;
    }
    
    public void tick(){
        //movimientos del player
        if(up){
            y -= SPEED;
        }
        if(dn){
            y +=SPEED;
        }
        if(lt){
            x -= SPEED;
        }
        if(rt){
            x += SPEED;
        }
    }
    
    public void render(Graphics g){
        g.drawImage(im.player, x, y, 16 * GameRPG.SCALE, 16 * GameRPG.SCALE, null);
    }

        
    

}

/** TODO / NOTAS **
 *
 */